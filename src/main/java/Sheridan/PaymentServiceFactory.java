/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Sheridan;

/**
 *
 * @author 123
 */
public class PaymentServiceFactory {
    private static PaymentServiceFactory instance = null;
    
    private PaymentServiceFactory( ) {
        
    }
    
    public PaymentService getPaymentService( PaymentServiceType type ) {
        switch ( type ) {
            case CREDIT : return new CreditPaymentService( );
            case DEBIT : return new DebitPaymentService( );
        }
        return null;
    }
    
    
    public static PaymentServiceFactory getInstance( ) {
        if ( instance == null ) {
            instance = new PaymentServiceFactory( );
        }
        return instance;
    }
}


