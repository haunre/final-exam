/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Sheridan;

/**
 *
 * @author 123
 */
public class DebitPaymentService extends PaymentService {
    public void processPayment( double amount ) {
        System.out.println( "Processing debit payment of $" + amount );
    }
        
}
